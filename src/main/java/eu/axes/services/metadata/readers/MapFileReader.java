package eu.axes.services.metadata.readers;


import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.format.ISODateTimeFormat;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;
import org.purl.dc.elements.DublinCoreAnnotator;

import ch.ebu.www.ebucore.EBUCoreAnnotator;

/**
 * This abstract class is a place holder for the way map files loaded from JSON or properties for instance can be converted into DublinCore+EbuCore statements.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
public abstract class MapFileReader extends MetadataFileReader {


	protected static final String PUBLISHER = "publisher";


	protected static final String DATE = "date";


	protected static final String IDENTIFIER = "identifier";


	protected static final String SOURCE = "source";


	protected static final String COVERAGE = "coverage";


	protected static final String RELATION = "relation";


	protected static final String FORMAT = "format";


	protected static final String SUBJECT = "subject";


	protected static final String RIGHTS = "rights";


	protected static final String TYPE = "type";


	protected static final String CONTRIBUTOR = "contributor";


	protected static final String CREATOR = "creator";


	protected static final String LANGUAGE = "language";


	protected static final String DESCRIPTION = "description";


	protected static final String TITLE = "title";


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 */
	public MapFileReader(final List<String> folders, final String extension) {
		super(folders, extension);
	}


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 * @param destDirectoryIfFoundAside
	 *            The name of the folder to be created and to which move the metadata file if found aside.
	 */
	public MapFileReader(final List<String> folders, final String extension, final String destDirectoryIfFoundAside) {
		super(folders, extension, destDirectoryIfFoundAside);
	}


	/**
	 * @param metadataLocation
	 *            The file to be loaded and converted into a map of properties.
	 * @return The map of properties to be annotated.
	 */
	protected abstract Map<String, String> loadMapFromFile(final File metadataLocation);



	@Override
	protected Annotation enrichResourceFromFile(final Resource resource, final File metadataLocation) {
		final Map<String, String> loadedProperties = this.loadMapFromFile(metadataLocation);
		if (loadedProperties.isEmpty()) {
			return null;
		}

		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(resource);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(URI.create(resource.getUri()), annot);
		final EBUCoreAnnotator ebuca = new EBUCoreAnnotator(URI.create(resource.getUri()), annot);

		final String title = loadedProperties.get(MapFileReader.TITLE);
		if ((title != null) && !title.trim().isEmpty()) {
			ebuca.writeMainTitle(title.trim());
		}
		final String description = loadedProperties.get(MapFileReader.DESCRIPTION);
		if ((description != null) && !description.trim().isEmpty()) {
			dca.writeDescription(description.trim());
		}
		final String language = loadedProperties.get(MapFileReader.LANGUAGE);
		if ((language != null) && !language.trim().isEmpty()) {
			dca.writeLanguage(language.trim());
		}

		MapFileReader.annotateContributors(ebuca, loadedProperties.get(MapFileReader.CREATOR), loadedProperties.get(MapFileReader.CONTRIBUTOR));
		MapFileReader.annotateGenres(ebuca, loadedProperties.get(MapFileReader.TYPE));
		MapFileReader.annotateSubjects(ebuca, loadedProperties.get(MapFileReader.SUBJECT));
		MapFileReader.annotateRights(ebuca, loadedProperties.get(MapFileReader.RIGHTS));

		this.logNotHandled(MapFileReader.FORMAT, loadedProperties.get(MapFileReader.FORMAT), resource.getUri());
		this.logNotHandled(MapFileReader.RELATION, loadedProperties.get(MapFileReader.RELATION), resource.getUri());
		this.logNotHandled(MapFileReader.COVERAGE, loadedProperties.get(MapFileReader.COVERAGE), resource.getUri());
		this.logNotHandled(MapFileReader.SOURCE, loadedProperties.get(MapFileReader.SOURCE), resource.getUri());
		this.logNotHandled(MapFileReader.IDENTIFIER, loadedProperties.get(MapFileReader.IDENTIFIER), resource.getUri());

		this.annotatePublication(ebuca, loadedProperties.get(MapFileReader.DATE), loadedProperties.get(MapFileReader.PUBLISHER));

		return annot;
	}


	/**
	 * @param ebuca
	 *            The ebcuore annotator opened on the resource
	 * @param rights
	 *            The rights as provided in the external file
	 */
	private static void annotateSubjects(final EBUCoreAnnotator ebuca, final String subjects) {
		int i = 0;
		if ((subjects != null) && !subjects.trim().isEmpty()) {
			for (final String subject : subjects.split(";")) {
				if (!subject.trim().isEmpty()) {
					final URI subjectUri = URI.create(ebuca.getSubject().toASCIIString() + "#Subject_" + i++);
					ebuca.writeSubject(subjectUri);
					ebuca.startInnerAnnotatorOn(subjectUri);
					ebuca.writeLabel(subject.trim());
					ebuca.endInnerAnnotator();
				}
			}
		}
	}


	/**
	 * @param ebuca
	 *            The ebcuore annotator opened on the resource
	 * @param rights
	 *            The rights as provided in the external file
	 */
	private static void annotateRights(final EBUCoreAnnotator ebuca, final String rights) {
		if ((rights != null) && !rights.trim().isEmpty()) {
			final URI rightsUri = URI.create(ebuca.getSubject().toASCIIString() + "#Rights_1");
			ebuca.writeRightsHolder(rightsUri);
			ebuca.startInnerAnnotatorOn(rightsUri);
			ebuca.writeLabel(rights.trim());
			ebuca.endInnerAnnotator();
		}
	}



	/**
	 * @param ebuca
	 *            The ebucore annotator opened on the document
	 * @param type
	 *            The type value as extracted from the external file
	 */
	private static void annotateGenres(final EBUCoreAnnotator ebuca, final String type) {
		int i = 0;
		if ((type != null) && !type.trim().isEmpty()) {
			for (final String genre : type.split(";")) {
				if (!genre.trim().isEmpty()) {
					final URI genreUri = URI.create(ebuca.getSubject().toASCIIString() + "#Genre_" + i++);
					ebuca.writeGenre(genreUri);
					ebuca.startInnerAnnotatorOn(genreUri);
					ebuca.writeLabel(genre.trim());
					ebuca.writeGenreName(genre.trim());
					ebuca.endInnerAnnotator();
				}
			}
		}
	}



	/**
	 * @param ebuca
	 *            The ebucore annotator
	 * @param date
	 *            The publication date as provided in the file
	 * @param publisher
	 *            The publisher value as provided by the file
	 */
	private void annotatePublication(final EBUCoreAnnotator ebuca, final String date, final String publisher) {
		Date publicationDate;
		final String resUri = ebuca.getSubject().toASCIIString();
		if ((date != null) && !date.trim().isEmpty()) {
			try {
				publicationDate = ISODateTimeFormat.dateTimeParser().parseDateTime(date).toDate();
			} catch (final Exception e) {
				this.logger.warn("Unable to read date " + date + " from video " + resUri + ". Using now.", e);
				publicationDate = Calendar.getInstance().getTime();
			}
		} else {
			this.logger.warn("No publication date provided for video " + resUri + ". Using now.");
			publicationDate = Calendar.getInstance().getTime();
		}


		final URI publiEventUri = URI.create(resUri + "#PublicationEvent_1");
		ebuca.writePubliEvent(publiEventUri);
		ebuca.startInnerAnnotatorOn(publiEventUri);
		ebuca.writePublished(publicationDate);

		if ((publisher != null) && !publisher.trim().isEmpty()) {
			final URI publiChannelUri = URI.create(resUri + "#PublicationChannel_1");
			ebuca.writePubliChannel(publiChannelUri);
			ebuca.startInnerAnnotatorOn(publiChannelUri);
			ebuca.writeLabel(publisher.trim());
			ebuca.endInnerAnnotator();
		}
		ebuca.endInnerAnnotator();
	}


	/**
	 * @param ebuca
	 *            The ebucore annotator opened on the document
	 * @param creator
	 *            The creator value as extracted from the external file
	 * @param contributor
	 *            The contributor value as extracted from the external file
	 */
	private static void annotateContributors(final EBUCoreAnnotator ebuca, final String creator, final String contributor) {
		final List<String> contributors = new LinkedList<>();
		if ((creator != null) && !creator.trim().isEmpty()) {
			contributors.addAll(Arrays.asList(creator.split(";")));
		}

		if ((contributor != null) && !contributor.trim().isEmpty()) {
			contributors.addAll(Arrays.asList(contributor.split(";")));
		}

		int i = 0;
		for (final String contrib : contributors) {
			if (!contrib.trim().isEmpty()) {
				final URI contribUri = URI.create(ebuca.getSubject().toASCIIString() + "#Contributor_" + i++);
				ebuca.writeContributor(contribUri);
				ebuca.startInnerAnnotatorOn(contribUri);
				ebuca.writeLabel(contrib.trim());
				ebuca.endInnerAnnotator();
			}
		}
	}


	protected void logNotHandled(final String key, final String value, final String id) {
		if ((value != null) && !value.trim().isEmpty()) {
			this.logger.warn(key + " property will not be taken into account for video " + id);
		}
	}

}