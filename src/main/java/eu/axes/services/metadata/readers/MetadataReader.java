package eu.axes.services.metadata.readers;

import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;



/**
 * The contract of the classes in charge of enriching an incoming Resource with metadata that are located outside of it (basically in a file).
 *
 * @author ymombrun
 * @date 2014-11-21
 */
public interface MetadataReader {


	/**
	 * @param resource
	 *            The resource to be enriched.
	 * @param videoId
	 *            The id of the video to be enriched.
	 * @return The annotation that contains the enrichment.
	 */
	public Annotation enrichResource(final Resource resource, final String videoId);

}
