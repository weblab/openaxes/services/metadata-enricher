package eu.axes.services.metadata.readers;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;



/**
 * This file is in charge of reading a JSON file containing dublin core metadata.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
public class JSONFileReader extends MapFileReader {


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 */
	public JSONFileReader(final List<String> folders, final String extension) {
		super(folders, extension);
	}


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 * @param destDirectoryIfFoundAside
	 *            The name of the folder to be created and to which move the metadata file if found aside.
	 */
	public JSONFileReader(final List<String> folders, final String extension, final String destDirectoryIfFoundAside) {
		super(folders, extension, destDirectoryIfFoundAside);
	}


	@Override
	protected Map<String, String> loadMapFromFile(final File metadataLocation) {
		final String json;
		try {
			json = FileUtils.readFileToString(metadataLocation);
		} catch (final IOException ioe) {
			this.logger.error("Unable to read file " + metadataLocation, ioe);
			return Collections.emptyMap();
		}
		final Map<String, String> map;
		try {
			map = new Gson().fromJson(json, Map.class);
		} catch (final JsonSyntaxException jse) {
			this.logger.error("File " + metadataLocation + " contains invalid JSON.", jse);
			return Collections.emptyMap();
		}
		if (map.isEmpty()) {
			this.logger.warn("Not a single property loaded from file " + metadataLocation);
		}
		return map;
	}


}
