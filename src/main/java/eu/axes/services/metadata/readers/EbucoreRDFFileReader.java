package eu.axes.services.metadata.readers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.utils.BackEndJenaHelper;
import org.ow2.weblab.utils.RDFHelperException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.util.ResourceUtils;


/**
 * Based on old MetadataReader code (AXES V1).
 * Loads a Jena model from an RDF file containing ebucore properties and enrich the resource with values found in the model.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
public class EbucoreRDFFileReader extends MetadataFileReader {


	/**
	 * The default prefix to be used when annotating ebucore properties.
	 */
	private static final String EBUCORE_PREFIX = "ebu";


	private static final String EBUCORE_URI = "http://www.ebu.ch/metadata/ontologies/ebucore#";


	/**
	 * @param folders
	 *            The folder into which to lookup for ebucore files
	 * @param extension
	 *            The extension of the ebucore file
	 */
	public EbucoreRDFFileReader(final List<String> folders, final String extension) {
		super(folders, extension);
	}


	/**
	 * @param folders
	 *            The folder into which to lookup for ebucore files
	 * @param extension
	 *            The extension of the ebucore file
	 * @param destDirectoryIfFoundAside
	 *            The name of the folder to be created and to which move the metadata file if found aside.
	 */
	public EbucoreRDFFileReader(final List<String> folders, final String extension, final String destDirectoryIfFoundAside) {
		super(folders, extension, destDirectoryIfFoundAside);
	}


	@Override
	protected Annotation enrichResourceFromFile(final Resource resource, final File metadataLocation) {
		final Model m;
		try {
			m = BackEndJenaHelper.modelFromFile(metadataLocation, EbucoreRDFFileReader.EBUCORE_URI, null);
		} catch (final IOException ioe) {
			this.logger.warn("Unable to load RDF model from file " + metadataLocation.getPath(), ioe);
			return null;
		}
		if (m.isEmpty()) {
			this.logger.warn("RDF model loaded from file " + metadataLocation.getPath() + " is empty.");
			return null;
		}
		ResIterator resIt = m.listResourcesWithProperty(m.getProperty(EbucoreRDFFileReader.EBUCORE_URI + "filename"));
		try {
			if (resIt.hasNext()) {
				final com.hp.hpl.jena.rdf.model.Resource subject = resIt.nextResource();
				if (!subject.isURIResource() || !subject.getURI().equals(resource.getUri())) {
					this.logger.info("Loaded metadata (" + metadataLocation.getAbsolutePath() + ")" + "uses a bad URI ('" + subject.getURI() + "' instead of " + resource.getUri() + "'). Replace it.");
					ResourceUtils.renameResource(subject, resource.getUri());
				}
			} else {
				resIt.close();
				this.logger.warn("Filename property not found in metadata file " + metadataLocation.getAbsolutePath() + ". Try with mainTitle");
				resIt = m.listResourcesWithProperty(m.getProperty(EbucoreRDFFileReader.EBUCORE_URI + "mainTitle"));
				if (resIt.hasNext()) {
					final com.hp.hpl.jena.rdf.model.Resource subject = resIt.nextResource();
					if (!subject.isURIResource() || !subject.getURI().equals(resource.getUri())) {
						this.logger.info("Loaded metadata uses a bad URI ('" + subject.getURI() + "' instead of " + resource.getUri() + "'). Replace it.");
						ResourceUtils.renameResource(subject, resource.getUri());
					}
				} else {
					this.logger.warn("Neither file name nor mainTitle found.");
				}

			}
			resIt.close();
			resIt = m.listSubjects();
			while (resIt.hasNext()) {
				final com.hp.hpl.jena.rdf.model.Resource subject = resIt.nextResource();
				if (subject.isAnon()) {
					ResourceUtils.renameResource(subject, resource.getUri() + "#" + System.nanoTime());
				}
			}
			resIt.close();
		} finally {
			resIt.close();
		}

		// Clean up the prefixes and write the annotation
		final BackEndJenaHelper backEndJenaHelper = new BackEndJenaHelper(m);
		backEndJenaHelper.getModel().setNsPrefix(EbucoreRDFFileReader.EBUCORE_PREFIX, EbucoreRDFFileReader.EBUCORE_URI);
		backEndJenaHelper.getModel().removeNsPrefix("");
		backEndJenaHelper.getModel().removeNsPrefix("j.0");
		backEndJenaHelper.getModel().removeNsPrefix("j.1");
		backEndJenaHelper.getModel().removeNsPrefix("j.2");
		final Annotation annotation = WebLabResourceFactory.createAndLinkAnnotation(resource);
		try {
			PoKUtil.setPoKData(annotation, backEndJenaHelper.getRdfXml());
		} catch (final RDFHelperException rdfhe) {
			this.logger.warn("RDF/XML loaded from file " + metadataLocation + " is not valid.");
			resource.getAnnotation().remove(annotation);
			return null;
		} catch (final Exception e) {
			this.logger.error("Error when serializing model into Resource " + resource.getUri(), e);
			resource.getAnnotation().remove(annotation);
			return null;
		}

		return annotation;
	}


}
