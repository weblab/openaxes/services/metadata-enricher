package eu.axes.services.metadata.readers;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;



/**
 * This abstract class that implements the MetadataReader contract for every implementations that are looking into a file to get the metadata to be added on a Resource.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
public abstract class MetadataFileReader implements MetadataReader {


	protected static final String DEFAULT_DEST = ".camel";


	/**
	 * The logger to be used inside.
	 */
	protected final Log logger;


	/**
	 * The list of folders to look into (in addition to the folder containing the original file).
	 */
	protected final List<File> folders;


	/**
	 * The extension (without dot) to look for.
	 */
	protected final String extension;


	/**
	 * The name of the directory to be created a to which move used metadata file if not null.
	 */
	protected final String destDirectoryIfFoundAside;


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 */
	public MetadataFileReader(final List<String> folders, final String extension) {
		this(folders, extension, DEFAULT_DEST);
	}


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 * @param destDirectoryIfFoundAside
	 *            The name of the directory to be created a to which move used metadata file if not null
	 */
	public MetadataFileReader(final List<String> folders, final String extension, final String destDirectoryIfFoundAside) {
		this.logger = LogFactory.getLog(this.getClass());
		this.folders = new LinkedList<>();
		this.extension = extension;
		for (final String folder : folders) {
			this.folders.add(new File(folder));
		}
		this.destDirectoryIfFoundAside = destDirectoryIfFoundAside;
	}


	/**
	 * @param resource
	 *            The resource to be enriched with metadata.
	 * @param metadataLocation
	 *            The metadata file to be read.
	 * @return The annotation that contains the enrichment, <code>null</code> if nothing has been added.
	 */
	protected abstract Annotation enrichResourceFromFile(final Resource resource, final File metadataLocation);


	@Override
	public Annotation enrichResource(final Resource resource, final String videoId) {
		this.logger.debug("Try to enrich resource " + videoId + " with " + this.getClass().getName());
		final File metadataLocation = this.findMetadataFile(resource, videoId);
		if (metadataLocation == null) {
			this.logger.info("No metadata of type " + this.toString() + " for " + videoId + ".");
			return null;
		}
		this.logger.debug("File " + metadataLocation.getAbsolutePath() + " found for metadata type " + this.toString() + " and video " + videoId + ".");
		final Annotation annot = this.enrichResourceFromFile(resource, metadataLocation);
		if ((annot != null) && (this.destDirectoryIfFoundAside != null) && !this.folders.contains(metadataLocation.getParentFile())) {
			final File destDir = new File(metadataLocation.getParentFile(), this.destDirectoryIfFoundAside);
			try {
				FileUtils.moveToDirectory(metadataLocation, destDir, true);
			} catch (final IOException ioe) {
				this.logger.warn("Unable to move file " + metadataLocation + " to destination directory " + destDir + ".", ioe);
			}
		}
		return annot;
	}


	/**
	 * Checks if a file basename.extension can be found aside the original source or in one of the folders that have been provided at initialisation time.
	 *
	 * @param resource
	 *            The resource to be used to get dc:source if available.
	 * @param videoId
	 *            The id of the video, used as basename for the metadata file.
	 * @return The file if found, <code>null</code> otherwise.
	 */
	protected File findMetadataFile(final Resource resource, final String videoId) {
		final String originalBasename;
		final Value<String> originalFilename = new WProcessingAnnotator(resource).readOriginalFileName();
		if (originalFilename.hasValue()) {
			originalBasename = FilenameUtils.getBaseName(originalFilename.firstTypedValue());
		} else {
			originalBasename = null;
		}

		final Value<String> sourceValue = new DublinCoreAnnotator(resource).readSource();
		File possibleMetadataFile = null;
		if (sourceValue.hasValue()) {
			final File sourceFolder = new File(sourceValue.firstTypedValue()).getParentFile().getAbsoluteFile();
			possibleMetadataFile = this.lookForFileInTheFolder(originalBasename, sourceFolder);
			if (possibleMetadataFile != null) {
				return possibleMetadataFile;
			}
			possibleMetadataFile = this.lookForFileInTheFolder(videoId, sourceFolder);
		}

		if (possibleMetadataFile != null) {
			return possibleMetadataFile;
		}

		final Iterator<File> iterator = this.folders.iterator();
		while (iterator.hasNext() && (possibleMetadataFile == null)) {
			final File folder = iterator.next();
			possibleMetadataFile = this.lookForFileInTheFolder(originalBasename, folder);
			if (possibleMetadataFile != null) {
				return possibleMetadataFile;
			}
			possibleMetadataFile = this.lookForFileInTheFolder(videoId, folder);
		}

		return possibleMetadataFile;
	}


	/**
	 * @param basename
	 *            The basename of the video and of the metadata
	 * @param rootFolder
	 *            The root folder into which to look for: basename.ext and basename/basename.ext
	 * @return The file if found or null
	 */
	protected File lookForFileInTheFolder(final String basename, final File rootFolder) {
		if (basename == null) {
			return null;
		}
		if (MetadataFileReader.checkFolder(rootFolder)) {
			final File inRootFile = new File(rootFolder, basename + "." + this.extension);
			if (MetadataFileReader.checkFile(inRootFile)) {
				return inRootFile;
			}
			final File subFolder = new File(rootFolder, basename);
			if (MetadataFileReader.checkFolder(subFolder)) {
				final File inSubFolderFile = new File(subFolder, basename + "." + this.extension);
				if (MetadataFileReader.checkFile(inSubFolderFile)) {
					return inSubFolderFile;
				}
			}
		}
		return null;
	}


	/**
	 * @param inRootFile
	 * @return
	 */
	private static boolean checkFile(final File inRootFile) {
		return inRootFile.exists() && inRootFile.isFile() && inRootFile.canRead() && (inRootFile.length() > 0);
	}


	/**
	 * @param rootFolder
	 * @return
	 */
	private static boolean checkFolder(final File rootFolder) {
		return rootFolder.exists() && rootFolder.isDirectory();
	}


	@Override
	public String toString() {
		return "[" + this.getClass().getSimpleName() + " on '." + this.extension + "' files]";
	}

}
