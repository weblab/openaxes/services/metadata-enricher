/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package eu.axes.services.metadata.readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


/**
 * @author ymombrun
 * @date 2014-11-21
 */
public class SimpleDCXMLFileReader extends MapFileReader {


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 */
	public SimpleDCXMLFileReader(final List<String> folders, final String extension) {
		super(folders, extension);
	}


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 * @param destDirectoryIfFoundAside
	 *            The name of the folder to be created and to which move the metadata file if found aside.
	 */
	public SimpleDCXMLFileReader(final List<String> folders, final String extension, final String destDirectoryIfFoundAside) {
		super(folders, extension, destDirectoryIfFoundAside);
	}


	@Override
	protected Map<String, String> loadMapFromFile(final File metadataLocation) {

		final Map<String, String> map = new HashMap<>();
		try (FileInputStream is = new FileInputStream(metadataLocation);) {
			final XMLEventReader reader;
			try {
				reader = XMLInputFactory.newFactory().createXMLEventReader(is);
			} catch (final XMLStreamException xmlse) {
				this.logger.error("Metadata file " + metadataLocation + " is not a valid XML file.", xmlse);
				return map;
			} catch (final FactoryConfigurationError fce) {
				this.logger.error("Unable to create XML fatory to read metadata file " + metadataLocation + ".", fce);
				return map;
			}
			while (reader.hasNext()) {
				try {
					final XMLEvent event = reader.nextEvent();
					if (event.isStartElement()) {
						final StartElement start = event.asStartElement();
						final String tag = start.getName().getLocalPart();
						if (reader.hasNext()) {
							final XMLEvent innerNext = reader.nextEvent();
							if (innerNext.isCharacters()) {
								final String value = innerNext.asCharacters().getData().trim();
								if (!value.isEmpty()) {
									if (map.containsKey(tag)) {
										// In case of multiple creator for instance, XML produces multiple tags. Need to concatenate them in the map.
										map.put(tag, map.get(tag) + ';' + value);
									} else {
										map.put(tag, value);
									}
								}
							}
						}
					}
				} catch (final Exception e) {
					this.logger.warn("An error occured while reading a tag in metadata file" + metadataLocation + ".", e);
				}
			}
		} catch (final FileNotFoundException fnfe) {
			this.logger.error("Metadata file " + metadataLocation + " not found.", fnfe);
		} catch (final IOException ioe) {
			// Nothing to do. It is the auto close error. We do not care.
		}

		return map;
	}
}
