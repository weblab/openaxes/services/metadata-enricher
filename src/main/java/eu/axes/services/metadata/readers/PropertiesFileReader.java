package eu.axes.services.metadata.readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * Implementation of the property reader interface in charge of extracting dublin core data from a Java standard property file.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
public class PropertiesFileReader extends MapFileReader {


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 */
	public PropertiesFileReader(final List<String> folders, final String extension) {
		super(folders, extension);
	}


	/**
	 * @param folders
	 *            The path to the folders to be watched.
	 * @param extension
	 *            The extension (without dot) to look for.
	 * @param destDirectoryIfFoundAside
	 *            The name of the folder to be created and to which move the metadata file if found aside.
	 */
	public PropertiesFileReader(final List<String> folders, final String extension, final String destDirectoryIfFoundAside) {
		super(folders, extension, destDirectoryIfFoundAside);
	}


	@Override
	protected Map<String, String> loadMapFromFile(final File metadataLocation) {
		final Properties props = new Properties();
		try (final FileInputStream fis = new FileInputStream(metadataLocation)) {
			props.load(fis);
		} catch (final IOException ioe) {
			this.logger.error("Unable to load properties from file " + metadataLocation + ".", ioe);
			return Collections.emptyMap();
		}

		final Map<String, String> map = new HashMap<>();
		for (final String key : props.stringPropertyNames()) {
			map.put(key, props.getProperty(key));
		}
		if (map.isEmpty()) {
			this.logger.warn("Not a single property loaded from file " + metadataLocation);
		}
		return map;
	}


}
