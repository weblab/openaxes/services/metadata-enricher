package eu.axes.services.metadata;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;

import ch.ebu.www.ebucore.EBUCoreAnnotator;
import eu.axes.services.metadata.readers.MetadataReader;
import eu.axes.utils.AxesAnnotator;
import eu.axes.utils.NamingSchemeUtils;

/**
 * Service in charge of reading metadata from various kind of files (using any implementation of MetadataReader) and to add the result to the input <code>Resource</code>.
 *
 * @author ymombrun
 * @date 2014-11-21
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class MetadataReaderService implements Analyser {


	/**
	 * The <code>Log</code> to be used.
	 */
	private final Log log;


	/**
	 * The list of readers in charge of extraction metadata from any source and to put the data back to the WebLab resource.
	 */
	private final List<? extends MetadataReader> readers;


	/**
	 * @param readers
	 *            The readers in charge of extraction metadata from any source and to put the data back to the WebLab resource.
	 */
	public MetadataReaderService(final List<? extends MetadataReader> readers) {
		super();
		this.log = LogFactory.getLog(this.getClass());
		this.readers = readers;
		if ((this.readers == null) || this.readers.isEmpty()) {
			this.log.error("Readers is null or empty. The metadata service will never work!");
		} else {
			this.log.debug("MetadataReader service started.");
		}
	}



	@Override
	public ProcessReturn process(final ProcessArgs args) throws InvalidParameterException, UnexpectedException {
		this.log.debug("Start of process method.");
		final String videoId = this.checkArgs(args);
		this.log.info("Beginning process with Resource: " + videoId + ".");

		Annotation annot = null;
		final Iterator<? extends MetadataReader> iter = this.readers.iterator();
		while ((annot == null) && iter.hasNext()) {
			final MetadataReader reader = iter.next();
			annot = reader.enrichResource(args.getResource(), videoId);
		}
		if (annot == null) {
			this.log.warn("Not a single working metadata file found for " + videoId + ".");
			annot = WebLabResourceFactory.createAndLinkAnnotation(args.getResource());
		}

		this.addMissingValues(args.getResource(), annot);

		this.log.info("End of metadata processing for resource " + videoId);
		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(args.getResource());
		return pr;
	}


	private void addMissingValues(final Resource res, final Annotation annot) {
		final EBUCoreAnnotator ebuca = new EBUCoreAnnotator(URI.create(res.getUri()), annot);

		// Add title if missing (using dc:title or wlp:hasOriginalFileName)
		if (!ebuca.readMainTitle().hasValue()) {
			this.log.debug("No title for resource " + res.getUri() + ". Will add a default value.");
			final Value<String> title = new DublinCoreAnnotator(res).readTitle();
			if (title.hasValue()) {
				ebuca.writeMainTitle(title.firstTypedValue());
			} else {
				final Value<String> fileName = new WProcessingAnnotator(res).readOriginalFileName();
				if (fileName.hasValue()) {
					ebuca.writeMainTitle(fileName.firstTypedValue());
				} else {
					try {
						ebuca.writeMainTitle(NamingSchemeUtils.getAxesVideoId(new AxesAnnotator(res), res));
					} catch (final WebLabCheckedException wlce) {
						this.log.error("Error when reading naming scheme.", wlce);
						ebuca.writeMainTitle(res.getUri());
					}
				}
			}
		}
		// Add publication date if missing (using dct:issue, dct:created, dct:date, dc:date or today)
		final URI publiEventUri;
		final Value<URI> publiEventValue = ebuca.readPubliEvent();
		if (publiEventValue.hasValue() && !publiEventValue.firstTypedValue().toString().isEmpty()) {
			publiEventUri = publiEventValue.firstTypedValue();
		} else {
			publiEventUri = URI.create(res.getUri() + "#PublicationEvent_1");
			ebuca.writePubliEvent(publiEventUri);
		}
		ebuca.startInnerAnnotatorOn(publiEventUri);
		if (!publiEventValue.hasValue() || !ebuca.readPublished().hasValue()) {
			this.log.debug("No publication date for resource " + res.getUri() + ". Will add a default value.");
			final DCTermsAnnotator dcta = new DCTermsAnnotator(res);
			final Value<Date> issued = dcta.readIssued();
			final Value<Date> created = dcta.readCreated();
			final Value<Date> dctDate = dcta.readDate();
			if (issued.hasValue()) {
				ebuca.writePublished(issued.firstTypedValue());
			} else if (created.hasValue()) {
				ebuca.writePublished(created.firstTypedValue());
			} else if (dctDate.hasValue()) {
				ebuca.writePublished(dctDate.firstTypedValue());
			} else {
				final Value<Date> dcDate = new DublinCoreAnnotator(res).readDate();
				if (dcDate.hasValue()) {
					ebuca.writePublished(dcDate.firstTypedValue());
				} else {
					ebuca.writePublished(Calendar.getInstance().getTime());
				}
			}
		}

	}


	/**
	 * Checks if <code>args</code> contains list of text sections and returns it.
	 *
	 * @param args
	 *            The <code>ProcessArgs</code>
	 * @return The video id.
	 * @throws ProcessException
	 *             If <code>args</code> is <code>null</code>, contains a <code>Resource</code> that is <code>null</code> or not a <code>MediaUnit</code>, contains a <code>MediaUnit</code> that is not
	 *             a <code>Document</code>, or that its first child is not a <code>Video</code> or if video id cannot be read.
	 */
	private String checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource in ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!(res instanceof Document)) {
			final String message = "Resource (" + res.getUri() + ") in ProcessArgs was not a Document, but a " + res.getClass().getName() + ".";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		try {
			return NamingSchemeUtils.getVideoId(new AxesAnnotator(res), res);
		} catch (final WebLabCheckedException wlce) {
			final String message = "Video Id cannot be read on resource '" + res.getUri() + "' while it is compulsory.";
			this.log.error(message, wlce);
			throw ExceptionFactory.createInvalidParameterException(message, wlce);
		}
	}

}
