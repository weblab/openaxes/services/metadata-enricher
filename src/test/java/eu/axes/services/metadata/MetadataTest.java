package eu.axes.services.metadata;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

import ch.ebu.www.ebucore.EBUCoreAnnotator;
import eu.axes.services.metadata.readers.EbucoreRDFFileReader;
import eu.axes.services.metadata.readers.JSONFileReader;
import eu.axes.services.metadata.readers.PropertiesFileReader;
import eu.axes.services.metadata.readers.SimpleDCXMLFileReader;

/**
 * @author ymombrun
 * @date 2014-11-21
 */
@SuppressWarnings({ "javadoc", "static-method" })
public class MetadataTest {


	private static final List<String> folders = Arrays.asList("/test/nonExistingFolder", "pom.xml", "src/test/resources/folderWithSubFolder", "src/test/resources/normalFolder");


	@Test
	public void fullCaseTest() throws Exception {
		final Date processTriggered = Calendar.getInstance().getTime();

		final Analyser service = new MetadataReaderService(Arrays.asList(new EbucoreRDFFileReader(MetadataTest.folders, "xml"), new EbucoreRDFFileReader(MetadataTest.folders, "rdf"),
				new SimpleDCXMLFileReader(MetadataTest.folders, "xml"), new JSONFileReader(Arrays.asList("src/test/resources/json"), "json"),
				new PropertiesFileReader(Arrays.asList("src/test/resources/properties"), "properties")));
		final WebLabMarshaller marshaller = new WebLabMarshaller(true);

		for (final File resFile : FileUtils.listFiles(new File("src/test/resources/resources"), new String[] { "xml" }, false)) {
			final Document doc = marshaller.unmarshal(resFile, Document.class);
			final ProcessArgs pa = new ProcessArgs();
			pa.setResource(doc);
			final ProcessReturn pr = service.process(pa);
			marshaller.marshalResource(pr.getResource(), new File("target", resFile.getName()));
			final EBUCoreAnnotator ebuca = new EBUCoreAnnotator(doc);
			final Value<String> mainTitle = ebuca.readMainTitle();
			Assert.assertTrue(doc.getUri(), mainTitle.hasValue());
			Assert.assertNotEquals(doc.getUri(), mainTitle.firstTypedValue(), doc.getUri());

			final Value<URI> pubEvent = ebuca.readPubliEvent();
			Assert.assertTrue(doc.getUri(), pubEvent.hasValue());
			ebuca.startInnerAnnotatorOn(pubEvent.firstTypedValue());
			final Value<Date> pubDate = ebuca.readPublished();
			Assert.assertTrue(doc.getUri(), pubDate.hasValue());
			if (doc.getUri().endsWith("009")) {
				Assert.assertTrue("Publication date is the not processing time for " + doc.getUri(), pubDate.firstTypedValue().after(processTriggered));
			} else {
				Assert.assertTrue("Publication date is the processing time for " + doc.getUri(), pubDate.firstTypedValue().before(processTriggered));
			}
			final Value<URI> pubChannel = ebuca.readPubliChannel();
			Assert.assertTrue(doc.getUri(), pubChannel.hasValue());
		}
	}


	@Test
	public void tryToLoadCXF() throws Exception {
		Assert.assertNotNull(new XmlBeanFactory(new FileSystemResource("src/main/webapp/WEB-INF/cxf-servlet.xml")).getBeansOfType(MetadataReaderService.class));
	}



}
